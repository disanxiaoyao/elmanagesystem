import { createRouter, createWebHistory } from 'vue-router'
import routes from './routers'

const API_BASE_URL = '/'
// const API_BASE_URL = process.env.VUE_APP_URL
// console.log('method API_BASE_URL', API_BASE_URL)
const router = createRouter({
  history: createWebHistory(API_BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title} | Cruise System`
  const role = localStorage.getItem('ms_username')
  if (!role && to.path !== '/login') {
    next('/login')
  } else if (to.meta.permission) {
    // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
    role === 'admin'
      ? next()
      : next('/403')
  } else {
    next()
  }
})

export default router
