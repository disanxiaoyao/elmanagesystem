import Home from '../views/Home'

// 首页工作台
const route1 = [
  {
    path: '/homePage',
    name: 'homePage',
    meta: {
      title: '系统首页'
    },
    component: () => import (
      /* webpackChunkName: "homePage" */
      '../views/homePage/Index')
  }
]
// 智能飞行
const route2 = [
  {
    path: '/flyLive',
    name: 'flyLive',
    meta: {
      title: '飞行直播'
    },
    component: () => import (
      /* webpackChunkName: "flyLive" */
      '../views/intelligentFly/FlyLive')
  },
  {
    path: '/flyTask',
    name: 'flyTask',
    meta: {
      title: '飞行任务'
    },
    component: () => import (
      /* webpackChunkName: "flyTask" */
      '../views/intelligentFly/FlyTask')
  },
  {
    path: '/trackTemplate',
    name: 'trackTemplate',
    meta: {
      title: '航线模板'
    },
    component: () => import (
      /* webpackChunkName: "trackTemplate" */
      '../views/intelligentFly/TrackTemplate')
  },
  {
    path: '/checkManage',
    name: 'checkManage',
    meta: {
      title: '检点管理'
    },
    component: () => import (
      /* webpackChunkName: "checkManage" */
      '../views/intelligentFly/CheckManage')
  },
]
// 线路管理
const route3 = [
  {
    path: '/trackManage',
    name: 'trackManage',
    meta: {
      title: '航迹管理'
    },
    component: () => import (
      /* webpackChunkName: "trackManage" */
      '../views/lineManage/TrackManage')
  },
  {
    path: '/lineImport',
    name: 'lineImport',
    meta: {
      title: '线路导入'
    },
    component: () => import (
      /* webpackChunkName: "lineImport" */
      '../views/lineManage/LineImport')
  },
  {
    path: '/cloudImport',
    name: 'cloudImport',
    meta: {
      title: '点云导入'
    },
    component: () => import (
      /* webpackChunkName: "cloudImport" */
      '../views/lineManage/CloudImport')
  }
]
// 数据管理
const route4 = [
  {
    path: '/dataImport',
    name: 'dataImport',
    meta: {
      title: '飞行直播'
    },
    component: () => import (
      /* webpackChunkName: "dataImport" */
      '../views/dataManage/DataImport')
  },
  {
    path: '/mapBrowse',
    name: 'mapBrowse',
    meta: {
      title: '地图浏览'
    },
    component: () => import (
      /* webpackChunkName: "mapBrowse" */
      '../views/dataManage/MapBrowse')
  },
  {
    path: '/picBrowse',
    name: 'picBrowse',
    meta: {
      title: '图片浏览'
    },
    component: () => import (
      /* webpackChunkName: "picBrowse" */
      '../views/dataManage/PicBrowse')
  },
  {
    path: '/statisticsView',
    name: 'statisticsView',
    meta: {
      title: '统计视图'
    },
    component: () => import (
      /* webpackChunkName: "statisticsView" */
      '../views/dataManage/StatisticsView.vue')
  }
]
// 智能分析
const route5 = [
  {
    path: '/defectAnalysis',
    name: 'defectAnalysis',
    meta: {
      title: '缺陷分析'
    },
    component: () => import (
      /* webpackChunkName: "defectAnalysis" */
      '../views/intelligentAnalysis/DefectAnalysis.vue')
  },
  {
    path: '/defectReview',
    name: 'defectReview',
    meta: {
      title: '缺陷审核'
    },
    component: () => import (
      /* webpackChunkName: "defectReview" */
      '../views/intelligentAnalysis/DefectReview.vue')
  },
  {
    path: '/defectReport',
    name: 'defectReport',
    meta: {
      title: '缺陷报告'
    },
    component: () => import (
      /* webpackChunkName: "defectReport" */
      '../views/intelligentAnalysis/DefectReport.vue')
  },
]
// 飞机管理
const route6 = [
  {
    path: '/airplaneId',
    name: 'airplaneId',
    meta: {
      title: '型号管理'
    },
    component: () => import (
      /* webpackChunkName: "airplaneId" */
      '../views/airplaneManage/AirplaneId.vue')
  },
  {
    path: '/airplaneReg',
    name: 'airplaneReg',
    meta: {
      title: '飞机注册'
    },
    component: () => import (
      /* webpackChunkName: "airplaneReg" */
      '../views/airplaneManage/AirplaneReg.vue')
  },
  {
    path: '/airplaneOut',
    name: 'airplaneOut',
    meta: {
      title: '出库管理'
    },
    component: () => import (
      /* webpackChunkName: "airplaneOut" */
      '../views/airplaneManage/AirplaneOut.vue')
  }
]
// 人员管理
const route7 = [
  {
    path: '/creatTeam',
    name: 'creatTeam',
    meta: {
      title: '创建班组'
    },
    component: () => import (
      /* webpackChunkName: "creatTeam" */
      '../views/peopleManage/CreatTeam.vue')
  },
  {
    path: '/creatPerson',
    name: 'creatPerson',
    meta: {
      title: '创建人员'
    },
    component: () => import (
      /* webpackChunkName: "creatPerson" */
      '../views/peopleManage/CreatPerson.vue')
  },
  {
    path: '/authoritySetting',
    name: 'authoritySetting',
    meta: {
      title: '权限设置'
    },
    component: () => import (
      /* webpackChunkName: "authoritySetting" */
      '../views/peopleManage/AuthoritySetting.vue')
  }
]
// elment 模板
const routeTemp = [
  {
    path: '/table',
    name: 'basetable',
    meta: {
      title: '表格'
    },
    component: () => import (
      /* webpackChunkName: "table" */
      '../views/zBaseTemp/BaseTable.vue')
  },
  {
    path: '/charts',
    name: 'basecharts',
    meta: {
      title: '图表'
    },
    component: () => import (
      /* webpackChunkName: "charts" */
      '../views/zBaseTemp/BaseCharts.vue')
  },
  {
    path: '/form',
    name: 'baseform',
    meta: {
      title: '表单'
    },
    component: () => import (
      /* webpackChunkName: "form" */
      '../views/zBaseTemp/BaseForm.vue')
  },
  {
    path: '/tabs',
    name: 'tabs',
    meta: {
      title: 'tab标签'
    },
    component: () => import (
      /* webpackChunkName: "tabs" */
      '../views/zBaseTemp/Tabs.vue')
  },
  {
    path: '/donate',
    name: 'donate',
    meta: {
      title: '鼓励作者'
    },
    component: () => import (
      /* webpackChunkName: "donate" */
      '../views/zBaseTemp/Donate.vue')
  },
  {
    path: '/permission',
    name: 'permission',
    meta: {
      title: '权限管理',
      permission: true
    },
    component: () => import (
      /* webpackChunkName: "permission" */
      '../views/zBaseTemp/Permission.vue')
  },
  {
    path: '/i18n',
    name: 'i18n',
    meta: {
      title: '国际化语言'
    },
    component: () => import (
      /* webpackChunkName: "i18n" */
      '../views/zBaseTemp/I18n.vue')
  },
  {
    path: '/upload',
    name: 'upload',
    meta: {
      title: '上传插件'
    },
    component: () => import (
      /* webpackChunkName: "upload" */
      '../views/zBaseTemp/Upload.vue')
  },
  {
    path: '/icon',
    name: 'icon',
    meta: {
      title: '自定义图标'
    },
    component: () => import (
      /* webpackChunkName: "icon" */
      '../views/zBaseTemp/Icon.vue')
  },
  {
    path: '/404',
    name: '404',
    meta: {
      title: '找不到页面'
    },
    component: () => import (/* webpackChunkName: "404" */
      '../views/404.vue')
  },
  {
    path: '/403',
    name: '403',
    meta: {
      title: '没有权限'
    },
    component: () => import (/* webpackChunkName: "403" */
      '../views/403.vue')
  }
]
// 主路由对象
const router = [
  {
    path: '/',
    redirect: '/homePage'
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      ...route1,...route2,
      ...route3,...route4,
      ...route5,...route6,
      ...route7,...routeTemp
    ]
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: '登录'
    },
    component: () => import (
      /* webpackChunkName: "login" */
      '../views/Login.vue')
  },
  {
    path: '/404',
    name: '404',
    meta: {
      title: '找不到页面'
    },
    component: () => import (/* webpackChunkName: "404" */
      '../views/404.vue')
  },
  {
    path: '/403',
    name: '403',
    meta: {
      title: '没有权限'
    },
    component: () => import (/* webpackChunkName: "403" */
      '../views/403.vue')
  }
]
export default [...router]
