export default {
  list :[
    {
      "icon": "el-icon-lx-home",
      "index": "homePage",
      "title": "首页工作台"
    },
    {
      "icon": "el-icon-lx-home",
      "index": "icon",
      "title": "icon"
    },
    // {
    //   "icon": "el-icon-lx-home",
    //   "index": "i18n",
    //   "title": "i18n"
    // },
    {
      "icon": "el-icon-lx-cascades",
      "index": "intelligentFly",
      "title": "智能飞行",
      "subs": [
        {
          "index": "flyLive",
          "title": "飞行直播"
        },
        {
          "index": "flyTask",
          "title": "飞行任务"
        },
        {
          "index": "trackTemplate",
          "title": "航线模板"
        },
        {
          "index": "checkManage",
          "title": "检点管理"
        }
      ]
    },
    {
      "icon": "el-icon-lx-sort",
      "index": "lineManage",
      "title": "线路管理",
      "subs": [
        {
          "index": "trackManage",
          "title": "航迹管理"
        },
        {
          "index": "lineImport",
          "title": "线路导入"
        },
        {
          "index": "cloudImport",
          "title": "点云导入"
        }]
    },
    {
      "icon": "el-icon-lx-edit",
      "index": "dataManage",
      "title": "数据管理",
      "subs": [
        {
          "index": "dataImport",
          "title": "导入数据"
        },
        {
          "index": "mapBrowse",
          "title": "地图浏览"
        },
        {
          "index": "picBrowse",
          "title": "图片浏览"
        },
        {
          "index": "statisticsView",
          "title": "统计视图"
        }
      ]
    },
    {
      "icon": "el-icon-lx-rank",
      "index": "intelligentAnalysis",
      "title": "智能分析",
      "subs": [
        {
          "index": "defectAnalysis",
          "title": "缺陷分析"
        },
        {
          "index": "defectReview",
          "title": "缺陷审核"
        },
        {
          "index": "defectReport",
          "title": "缺陷报告"
        }
      ]
    },
    {
      "icon": "el-icon-lx-info",
      "index": "airplaneManage",
      "title": "飞机管理",
      "subs": [
        {
          "index": "airplaneId",
          "title": "型号管理"
        },
        {
          "index": "airplaneReg",
          "title": "飞机注册"
        },
        {
          "index": "airplaneOut",
          "title": "出库管理"
        }
      ]
    },
    {
      "icon": "el-icon-lx-people",
      "index": "peopleManage",
      "title": "人员管理",
      "subs": [
        {
          "index": "creatTeam",
          "title": "创建班组"
        },
        {
          "index": "creatPerson",
          "title": "创建人员"
        },
        {
          "index": "authoritySetting",
          "title": "权限设置"
        }
      ]
    }
  ]
}
